## Version 
2.1
##Author 
Sven Eckelmann (eckelmann@htw-dresden.de) 

## Synopsis

Receiving CAN messages form the internal bmw bus system and interpret them 
in a appropriate way 

* subscribed topic for interpretation: 
/can1/received_messages
/can2/received_messages
    
translated data in topic /twizy_egodata
* ID 54       yaw_rate            [degree/s]          Decription from Bosch MM3  
* ID 1433     speed               [km/h]              Description from CAN_Twizy
* ID 192      steering angle      [degree]            Description from CAN_Twizy
* ID 54       lon_acc (laengs)    [m/s²]              Decription from Bosch MM3  
* ID 53       lat_acc (quer)      [m/s²]              Decription from Bosch MM3  

     
Publish ego data as twizy_egodata.msg 

## Motivation
Filter only relevant CAN messages for further projects 

## Dependencies:
 - can_msgs.msg 
 - ros_twizy_messages.msg  (build from "twizy_messages")
 

## Installation
Download the source in your /catkin/src directory and run catkin_make 
Run node with:
roslaunch ros_twizy_ego ego.py

##Hints
Please check todo in source code 
## Tests

 

    

